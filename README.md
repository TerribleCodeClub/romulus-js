# Romulus-JS
[![Build Status](https://drone.jacknet.io/api/badges/TerribleCodeClub/romulus-js/status.svg)](https://drone.jacknet.io/TerribleCodeClub/romulus-js) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

An implementation of the Romulus-M cryptography specification.

## Build

To build the Romulus-JS library, first clone this repository.

Run the following commands from the root of the repository:
```bash
$ npm install
$ npm run build
```
The build output will be saved to the `dist` directory.

## Development instructions

Requirements:
- The latest LTS builds of Node and npm.

Follow the instructions below to lint, test and build Romulus-JS.

#### Lint

```bash
$ npm install
$ npm run lint
```

#### Test

```bash
$ npm install
$ npm run test
```
#### Build

```bash
$ npm install
$ npm run build
```

### Visual Studio Code

This repository contains the necessary configuration files to debug, test and build Romulus-JS using only Visual Studio Code. 

Run the build task (`Ctrl+Shift+B` or `⇧⌘B`) to automatically compile the Typescript source files in the background.

Unit tests use the [Jest](https://jestjs.io/) library. Support for Visual Studio Code is offered through the [Jest marketplace package](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest) maintained by Orta.

## Contribution guidelines

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

This library uses [ts-standard](https://github.com/standard/ts-standard), based on [JavaScript Standard Style](https://standardjs.com/rules.html). Please ensure all contributions are ts-standard compliant before submitting a pull request.
