import { referenceTests } from './resources/reference-tests'
import { cryptoAeadDecrypt, cryptoAeadEncrypt, DecryptResult } from '../src/romulus-m'

function parseHexString (string: string): number[] {
  const ret = []
  for (let i = 0; i < string.length; i += 2) {
    ret.push(parseInt(string.slice(i, i + 2), 16))
  }

  return ret
}

test.each(referenceTests)('Perform encryption using reference test %#.', (key, nonce, plaintext, associatedData, ciphertext) => {
  // When
  const result = cryptoAeadEncrypt(parseHexString(plaintext), parseHexString(associatedData), parseHexString(nonce), parseHexString(key))

  // Then
  const expectedResult = parseHexString(ciphertext)
  expect(result).toMatchObject(expectedResult)
})

test.each(referenceTests)('Perform decryption using reference test %#.', (key, nonce, plaintext, associatedData, ciphertext) => {
  // When
  const result = cryptoAeadDecrypt(parseHexString(ciphertext), parseHexString(associatedData), parseHexString(nonce), parseHexString(key))

  // Then
  const expectedResult: DecryptResult = {
    success: true,
    plaintext: parseHexString(plaintext)
  }
  expect(result).toMatchObject(expectedResult)
})
