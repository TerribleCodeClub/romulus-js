import { decrypt } from '../src/decrypt'

test('Test nonce parsing by public decrypt function.', () => {
  // Given
  const ciphertext = Uint8Array.from([
    // Nonce
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    // Ciphertext
    225, 53, 3, 212, 22, 112, 246, 194, 61, 171, 230, 187, 157, 102, 32, 76, 62, 65,
    25, 202, 255, 201, 206, 49, 60, 58, 82, 216, 72, 116, 106, 129, 162, 142, 69, 40,
    167, 88, 94, 195, 174, 217, 242, 149, 224, 125, 196, 237, 172, 165, 116, 119, 128
  ])
  const associatedData = new TextEncoder().encode('Some associated data.')
  const key = Uint8Array.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f])

  // When
  const result = decrypt(ciphertext, associatedData, key)

  // Then
  const expectedResult = new TextEncoder().encode('Hello, World! This is a test message.')
  expect(result.success).toBe(true)
  expect(result.plaintext).toMatchObject(expectedResult)
})

test('Test decryption with an invalid key.', () => {
  // Given
  const ciphertext = Uint8Array.from([
    // Nonce
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    // Ciphertext
    225, 53, 3, 212, 22, 112, 246, 194, 61, 171, 230, 187, 157, 102, 32, 76, 62, 65,
    25, 202, 255, 201, 206, 49, 60, 58, 82, 216, 72, 116, 106, 129, 162, 142, 69, 40,
    167, 88, 94, 195, 174, 217, 242, 149, 224, 125, 196, 237, 172, 165, 116, 119, 128
  ])
  const associatedData = new TextEncoder().encode('Some associated data.')
  const key = Uint8Array.from([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

  // When
  const result = decrypt(ciphertext, associatedData, key)

  // Then
  expect(result.success).toBe(false)
  expect(result.plaintext).toMatchObject(new Uint8Array())
})
