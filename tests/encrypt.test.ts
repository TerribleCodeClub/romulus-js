import { decrypt } from '../src/decrypt'
import { encrypt } from '../src/encrypt'

test('Test nonce generation by public encrypt function.', () => {
  // Given
  const message = new TextEncoder().encode('Hello, World! This is a test message.')
  const associatedData = new TextEncoder().encode('Some associated data.')
  const key = Uint8Array.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f])

  // When
  const ciphertext = encrypt(message, associatedData, key)
  const plaintext = decrypt(ciphertext, associatedData, key)

  // Then
  expect(plaintext.success).toBe(true)
  expect(plaintext.plaintext).toMatchObject(message)
})
