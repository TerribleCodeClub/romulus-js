import { cryptoAeadDecrypt, cryptoAeadEncrypt } from '../src/romulus-m'

function stringToArray (string: string): number[] {
  const encoder = new TextEncoder()
  return Array.from(encoder.encode(string))
}

test('Encrypt a message with no associated data.', () => {
  // Given
  const message = stringToArray('Hello, World! This is a test message.')
  const associatedData = stringToArray('')
  const nonce = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')
  const key = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')

  // When
  const result = cryptoAeadEncrypt(message, associatedData, nonce, key)

  // Then
  const expectedResult = [
    85, 125, 23, 244, 73, 241, 140, 72, 166, 113, 114, 78, 239, 211, 84, 113, 222,
    153, 207, 183, 69, 142, 174, 15, 38, 46, 112, 162, 229, 27, 136, 184, 163, 78,
    132, 42, 107, 160, 74, 115, 28, 251, 209, 37, 48, 57, 184, 204, 199, 247, 93, 5, 208
  ]
  expect(result).toMatchObject(expectedResult)
})

test('Encrypt a message with associated data.', () => {
  // Given
  const message = stringToArray('Hello, World! This is a test message.')
  const associatedData = stringToArray('Some associated data.')
  const nonce = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')
  const key = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')

  // When
  const result = cryptoAeadEncrypt(message, associatedData, nonce, key)

  // Then
  const expectedResult = [
    225, 53, 3, 212, 22, 112, 246, 194, 61, 171, 230, 187, 157, 102, 32, 76, 62, 65,
    25, 202, 255, 201, 206, 49, 60, 58, 82, 216, 72, 116, 106, 129, 162, 142, 69, 40,
    167, 88, 94, 195, 174, 217, 242, 149, 224, 125, 196, 237, 172, 165, 116, 119, 128
  ]
  expect(result).toMatchObject(expectedResult)
})

test('Decrypt a message with no associated data.', () => {
  // Given
  const ciphertext = [
    85, 125, 23, 244, 73, 241, 140, 72, 166, 113, 114, 78, 239, 211, 84, 113, 222,
    153, 207, 183, 69, 142, 174, 15, 38, 46, 112, 162, 229, 27, 136, 184, 163, 78,
    132, 42, 107, 160, 74, 115, 28, 251, 209, 37, 48, 57, 184, 204, 199, 247, 93, 5, 208
  ]
  const associatedData = stringToArray('')
  const nonce = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')
  const key = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')

  // When
  const result = cryptoAeadDecrypt(ciphertext, associatedData, nonce, key)

  // Then
  const expectedResult = stringToArray('Hello, World! This is a test message.')
  expect(result.success).toBe(true)
  expect(result.plaintext).toMatchObject(expectedResult)
})

test('Decrypt a message with associated data.', () => {
  // Given
  const ciphertext = [
    225, 53, 3, 212, 22, 112, 246, 194, 61, 171, 230, 187, 157, 102, 32, 76, 62, 65,
    25, 202, 255, 201, 206, 49, 60, 58, 82, 216, 72, 116, 106, 129, 162, 142, 69, 40,
    167, 88, 94, 195, 174, 217, 242, 149, 224, 125, 196, 237, 172, 165, 116, 119, 128
  ]
  const associatedData = stringToArray('Some associated data.')
  const nonce = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')
  const key = stringToArray('\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')

  // When
  const result = cryptoAeadDecrypt(ciphertext, associatedData, nonce, key)

  // Then
  const expectedResult = stringToArray('Hello, World! This is a test message.')
  expect(result.success).toBe(true)
  expect(result.plaintext).toMatchObject(expectedResult)
})
