import { cryptoAeadEncrypt } from './romulus-m'
import { v4 as uuidv4 } from 'uuid'

/**
 * Encrypt a message using the Romulus-M encryption algorithm.
 * N.B. A nonce is automatically prepended to the ciphertext using this function.
 * @param message The plaintext message to encrypt.
 * @param associatedData The associated data.
 * @param key The encryption key.
 * @returns The nonce-prepended ciphertext.
 */
export function encrypt (message: Uint8Array, associatedData: Uint8Array, key: Uint8Array): Uint8Array {
  // Generate a nonce.
  const nonce = new Uint8Array(16)
  uuidv4({}, nonce)

  // Encrypt the data using the associated data, newly generated nonce and encryption key.
  const ciphertext = Uint8Array.from(cryptoAeadEncrypt(Array.from(message), Array.from(associatedData), Array.from(nonce), Array.from(key)))

  // Return the nonce-prepended ciphertext.
  return new Uint8Array([...nonce, ...ciphertext])
}
