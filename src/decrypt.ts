import { cryptoAeadDecrypt } from './romulus-m'

interface DecryptResult {
  success: boolean
  plaintext: Uint8Array
}

/**
 * Decrypt a Romulus-M encrypted message.
 * N.B. Nonces are handled automatically by this function.
 * @param buffer The nonce-prepended data to be decrypted.
 * @param associatedData The associated data.
 * @param key The encryption key.
 * @returns A decrypted DecryptResult object.
 */
export function decrypt (buffer: Uint8Array, associatedData: Uint8Array, key: Uint8Array): DecryptResult {
  // Split nonce from ciphertext.
  const nonce = Array.from(buffer.slice(0, 16))
  const ciphertext = Array.from(buffer.slice(16))

  // Decrypt ciphertext using the associated data, nonce and encryption key.
  const result = cryptoAeadDecrypt(ciphertext, Array.from(associatedData), nonce, Array.from(key))

  // Return the ciphertext and decryption status.
  return {
    success: result.success,
    plaintext: Uint8Array.from(result.plaintext)
  }
}
